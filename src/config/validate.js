const customDictionary = {
  en: {
    custom: {
      first_name: {
        required: 'Por favor ingresa tu nombre'
      },
      last_name: {
        required: 'Por favor ingresa tu apellido'
      },
      mobile_phone: {
        required: 'Por favor ingresa tu telefono'
      },
      address: {
        required: 'Por favor ingresa tu dirección'
      },
      company: {
        required: 'Por favor ingresa nombre de la compañia'
      },
      contact: {
        required: 'Por favor ingresa el nombre del contacto de referencia'
      }
    }
  }
}

const config = {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  dictionary: customDictionary,
  errorBagName: 'errors', // change if property conflicts
  events: 'input|blur',
  fieldsBagName: 'fields',
  i18n: null, // the vue-i18n plugin instance
  i18nRootKey: 'validations', // the nested key under which the validation messages will be located
  inject: true,
  locale: 'en',
  strict: true,
  validity: false
}

export default config
