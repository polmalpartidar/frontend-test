import VeeValidate from 'vee-validate'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import validateConfig from './config/validate'

import './styles/main.styl'
import './registerServiceWorker'

Vue.config.productionTip = false
// veeValidate
Vue.use(VeeValidate, validateConfig)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
