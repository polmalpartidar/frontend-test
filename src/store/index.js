import Vue from 'vue'
import Vuex from 'vuex'
import candidates from '../JSON/candidates.js'

let CANDIDATES = candidates
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    candidates: [],
    candidate: {}
  },
  getters: {
    candidates (state) {
      return state.candidates
    }
  },
  mutations: {
    setCandidates (state, data) {
      state.candidates = data
    },
    setCandidate (state, data) {
      state.candidate = data
    }
  },
  actions: {
    GETCANDITATES ({ commit }) {
      // aqui podemos agregar las peticiones al servidor
      return new Promise((resolve, reject) => {
        // here we can get data from api
        commit('setCandidates', JSON.parse(JSON.stringify(CANDIDATES)))
        resolve(CANDIDATES)
      })
    },
    GETCANDIDATE ({ commit }, payload) {
      // aqui podemos agregar las peticiones al servidor
      return new Promise((resolve, reject) => {
        const cand = CANDIDATES.find((cand) => cand.id === parseInt(payload))
        if (cand) {
          commit('setCandidate', JSON.parse(JSON.stringify(cand)))
          resolve(cand)
        } else {
          reject(new Error('not found'))
        }
      })
    },
    UPDATECANDIDATE ({ commit }, payload) {
      return new Promise((resolve, reject) => {
        let pos = CANDIDATES.findIndex((item) => item.id === parseInt(payload.id))
        if (!payload.id) { reject(new Error('candidate not found')) }
        try {
          CANDIDATES[pos].name = payload.data.name
          CANDIDATES[pos].last_name = payload.data.last_name
          CANDIDATES[pos].phone = payload.data.phone
          CANDIDATES[pos].address = payload.data.address
          resolve(true)
        } catch (err) {
          reject(new Error("error to save candidate's info "))
        }
      })
    },
    DELCANDIDATE ({ commit }, payload) {
      return new Promise((resolve, reject) => {
        if (!payload) { reject(new Error('candidate not found')) }
        let pos = CANDIDATES.findIndex((item) => item.id === parseInt(payload))
        try {
          CANDIDATES.splice(pos, 1)
          resolve(true)
        } catch (err) {
          reject(new Error('try again'))
        }
      })
    },
    CREATELAREF ({ commit }, payload) {
      return new Promise((resolve, reject) => {
        if (!payload.id) { reject(new Error('candidate not found')) }
        try {
          const { data } = payload
          let pos = CANDIDATES.findIndex((item) => item.id === parseInt(payload.id))
          data.id = CANDIDATES[pos].laboral_references.length
          CANDIDATES[pos].laboral_references.push(data)
          resolve(true)
        } catch (err) {
          reject(new Error('error to save'))
        }
      })
    },
    GETLABREF ({ commit }, payload) {
      // aqui podemos agregar las peticiones al servidor
      return new Promise((resolve, reject) => {
        const cand = CANDIDATES.find((cand) => cand.id === parseInt(payload.id))
        const RefLab = cand.laboral_references.find((ref) => ref.id === parseInt(payload.code))
        if (!cand && !RefLab) {
          reject(new Error('not found'))
        }
        resolve({ cand, ref: RefLab })
      })
    },
    DELREF ({ commit }, payload) {
      console.log(payload)
      return new Promise((resolve, reject) => {
        if (!payload.id) { reject(new Error('candidate not found')) }
        try {
          let pos = CANDIDATES.findIndex((item) => item.id === parseInt(payload.id))
          let posRef = CANDIDATES[pos].laboral_references.findIndex((ref) => ref.id === parseInt(payload.ref))
          CANDIDATES[pos].laboral_references.splice(posRef, 1)
          resolve(true)
        } catch (err) {
          reject(new Error('try again'))
        }
      })
    }
  },
  modules: {
  },
  strict: true
})
