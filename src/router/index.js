import Vue from 'vue'
import Router from 'vue-router'

const _import_ = file => () => import('@/views/' + file + '.vue')

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'Home', component: _import_('Home') },
    { path: '/palindromic', name: 'PalindromicNumber', component: _import_('Palindromic') },
    { path: '/pythagorean', name: 'PythagoreanNumber', component: _import_('Pythagorean') },
    { path: '/:id', name: 'Candidate', component: _import_('Candidate') },
    { path: '/:id/editar', name: 'EditCandidate', component: _import_('EditCandidate') },
    { path: '/:id/agregar-referencia', name: 'AgregarLabRef', component: _import_('CreateNewLaboralReference') },
    { path: '/:id/referencia/:code', name: 'EditLabRef', component: _import_('EditLaboralReference') },
    { path: '*', name: 'NoFound', component: _import_('NoFound') }
  ],
  mode: 'history'
})
