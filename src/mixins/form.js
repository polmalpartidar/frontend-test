export default {
  methods: {
    FI_onSubmit (scope, cb) {
      this.$validator.validateAll()
        .then((result) => {
          if (result && this.success) {
            this.success()
          } else if (this.failure) {
            this.failure()
          }
        })
    }
  }
}
